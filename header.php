<!DOCTYPE html>
<html lang="en">
<head>
    <title>findAll</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="font-awsome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <!--Top Bar Start-->
    <div class="topbar">
        <div class="addList">
            <i class="fa fa-plus-circle"></i> Add Listing
        </div>
        <div class="topbar-dropdown">
            <button class="topbar-dropbtn">English <i class="fa fa-angle-down"></i></button>
            <div class="topbar-dropdown-content">
                <a href="#">German</a>
                <a href="#">French</a>
                <a href="#">Italian</a>
            </div>
        </div>
    </div>
    <!--Header START-->
    <div class="header">
        <div class="headerLeft">
            <img src="https://findall.qodeinteractive.com/wp-content/uploads/2019/08/logo-standard.png" style="height:25px;" alt="logo">
            <!--<ul class="menuItems">-->
            <!--    <li>Home</li>-->
            <!--    <li>Pages</li>-->
            <!--    <li>Listings</li>-->
            <!--    <li>Pricing</li>-->
            <!--    <li>Blog</li>-->
            <!--    <li>Shop</li>-->
            <!--</ul>-->
            
            <div class="homemenu">
                <a href="">Home</a>
                <div class="homemenu-content">
                    <ul>
                        <li><a href="">Listing</a></li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="homemenu">
                <a href="">Listings</a>
                <div class="homemenu-content">
                    <ul>
                        <li><a href="">Listing</a></li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="homemenu">
                <a href="">Pricing</a>
            </div>
            <div class="homemenu">
                <a href="">Blog</a>
                <div class="homemenu-content">
                    <ul>
                        <li><a href="">Listing</a></li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="homemenu">
                <a href="">Shop</a>
                <div class="homemenu-content">
                    <ul>
                        <li><a href="">Listing</a></li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="homemenu">
                <a href="">Pages</a>
                <div class="homemenu-content">
                    <ul>
                        <li><a href="">Listing</a></li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                        <li class="homemenu-submenu"><a href="">By Location</a>
                            <ul>
                                <li><a href="">Veronica</a></li>
                                <li><a href="">d</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="headerRight">
            <div class="cart">
                <a href=""><i class="fa fa-shopping-cart"></i> Cart</a>
            </div>
            <div class="signIn">
                <a href=""><i class="fa fa-user"></i> Sign in</a>
            </div>
        </div>
    </div>
    
    <div class="mobileHeader">
        <div class="container">
            <div class="mobileHeaderTopbar">
                <div>
                    <img src="https://findall.qodeinteractive.com/wp-content/uploads/2019/08/logo-standard.png" style="height:25px;" alt="logo">
                </div>
                <div class="mobileHeaderRight">
                    <div class="mobileSignin">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="hamburger">
                        <i class="fa fa-bars"></i>
                    </div>
                </div>
            </div>
            <div class="mobileMenuItems">
                <div class="mobileMenuItem" id="mmItemOne">
                    <p class="m-0">Home</p>
                    <i class="fa fa-angle-down"></i>
                </div>
                <div id="mmItemsOne">
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                </div>
                <div class="mobileMenuItem" id="mmItemTwo">
                    <p class="m-0">Pages</p>
                    <i class="fa fa-angle-right"></i>
                </div>
                <div id="mmItemsTwo">
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                </div>
                <div class="mobileMenuItem" id="mmItemThree">
                    <p class="m-0">Listing</p>
                </div>
                <div id="mmItemsThree">
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                </div>
                <div class="mobileMenuItem" id="mmItemFour">
                    <p class="m-0">Pricing</p>
                    <i class="fa fa-angle-right"></i>
                </div>
                <div id="mmItemsFour">
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                </div>
                <div class="mobileMenuItem" id="mmItemFive">
                    <p class="m-0">Blog</p>
                    <i class="fa fa-angle-right"></i>
                </div>
                <div id="mmItemsFive">
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                </div>
                <div class="mobileMenuItem" id="mmItemSix">
                    <p class="m-0">Shop</p>
                    <i class="fa fa-angle-right"></i>
                </div>
                <div id="mmItemsSix">
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                    <li>Orange</li>
                </div>
            </div>
        </div>
    </div>
    
    <div class="banner">
        <div class="container">
            <div class="bannerFirstRow">
                <div class="col-xl-3 col-lg-3 col-md-3">
                    <input class="col-12" type="text" placeholder="What are you looking at?">
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3">
                    <select class="col-12">
                        <option>All Categories</option>
                    </select>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3">
                    <select class="col-12">
                        <option>All Locations</option>
                    </select>
                </div>
                <div class="col-xl-2  col-lg-3 col-md-3 col-4">
                    <button class="col-12 searchBtn">Search <i class="fa fa-search"></i></button>
                </div>
            </div>
            <div class="bannerSecondRow">
                <div>
                    <p style="margin:0;">Popular :</p>
                </div>
                <a href="">Automotive</a>
                <a href="">Beauty Salon</a>
                <a href="">Business</a>
                <a href="">Cleaning</a>
                <a href="">Plumber</a>
            </div>
        </div>
    </div>
    
    <div>
        <div class="cardBoxHeader">
            <div>
                <h2>Best Battery Phones</h2>
                <p class="m-0">More than 4000mAh</p>
            </div>
            <div class="viewAllBtn">
                <a href="">VIEW ALL</a>
            </div>
        </div>
        <div style="display:flex;padding:10px 30px;overflow:hidden;">
            <div>
                <div class="item">
                    <a title="5000 mAh Battery" target="_blank" href="">
                        <div style="height:150px">
                            <div style="height:150px;width:150px">
                                <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                            </div>
                        </div>
                        <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                        <p class="item-summary">5000 mAh Battery</p>
                        <p class="item-summary">Now ₹10999</p>
                    </a>
                </div>
            </div>
            
            <div>
                <div class="item">
                    <a title="5000 mAh Battery" target="_blank" href="">
                        <div style="height:150px">
                            <div style="height:150px;width:150px">
                                <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                            </div>
                        </div>
                        <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                        <p class="item-summary">5000 mAh Battery</p>
                        <p class="item-summary">Now ₹10999</p>
                    </a>
                </div>
            </div>
            
            <div>
                <div class="item">
                    <a title="5000 mAh Battery" target="_blank" href="">
                        <div style="height:150px">
                            <div style="height:150px;width:150px">
                                <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                            </div>
                        </div>
                        <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                        <p class="item-summary">5000 mAh Battery</p>
                        <p class="item-summary">Now ₹10999</p>
                    </a>
                </div>
            </div>
            
            <div>
                <div class="item">
                    <a title="5000 mAh Battery" target="_blank" href="">
                        <div style="height:150px">
                            <div style="height:150px;width:150px">
                                <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                            </div>
                        </div>
                        <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                        <p class="item-summary">5000 mAh Battery</p>
                        <p class="item-summary">Now ₹10999</p>
                    </a>
                </div>
            </div>
            
            <div>
                <div class="item">
                    <a title="5000 mAh Battery" target="_blank" href="">
                        <div style="height:150px">
                            <div style="height:150px;width:150px">
                                <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                            </div>
                        </div>
                        <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                        <p class="item-summary">5000 mAh Battery</p>
                        <p class="item-summary">Now ₹10999</p>
                    </a>
                </div>
            </div>
            
            <div>
                <div class="item">
                    <a title="5000 mAh Battery" target="_blank" href="">
                        <div style="height:150px">
                            <div style="height:150px;width:150px">
                                <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                            </div>
                        </div>
                        <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                        <p class="item-summary">5000 mAh Battery</p>
                        <p class="item-summary">Now ₹10999</p>
                    </a>
                </div>
            </div>
            
            <div>
                <div class="item">
                    <a title="5000 mAh Battery" target="_blank" href="">
                        <div style="height:150px">
                            <div style="height:150px;width:150px">
                                <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                            </div>
                        </div>
                        <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                        <p class="item-summary">5000 mAh Battery</p>
                        <p class="item-summary">Now ₹10999</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    <div>
        <div class="cardBoxHeader">
            <div>
                <h2>Deals of the Day</h2>
            </div>
            <div class="viewAllBtn">
                <a href="">VIEW ALL</a>
            </div>
        </div>
        <div style="display:flex;padding:10px 30px;">
            <div class="col-xl-10 col-lg-9 col-9 p-0" style="display:flex;overflow:hidden;">
                <div>
                    <div class="item">
                        <a title="5000 mAh Battery" target="_blank" href="">
                            <div style="height:150px;">
                                <div style="height:150px;width:150px;">
                                    <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                                </div>
                            </div>
                            <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                            <p class="item-summary">5000 mAh Battery</p>
                            <p class="item-summary">Now ₹10999</p>
                        </a>
                    </div>
                </div>
                
                <div>
                    <div class="item">
                        <a title="5000 mAh Battery" target="_blank" href="">
                            <div style="height:150px">
                                <div style="height:150px;width:150px;">
                                    <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                                </div>
                            </div>
                            <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                            <p class="item-summary">5000 mAh Battery</p>
                            <p class="item-summary">Now ₹10999</p>
                        </a>
                    </div>
                </div>
                
                <div>
                    <div class="item">
                        <a title="5000 mAh Battery" target="_blank" href="">
                            <div style="height:150px">
                                <div style="height:150px;width:150px">
                                    <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                                </div>
                            </div>
                            <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                            <p class="item-summary">5000 mAh Battery</p>
                            <p class="item-summary">Now ₹10999</p>
                        </a>
                    </div>
                </div>
                
                <div>
                    <div class="item">
                        <a title="5000 mAh Battery" target="_blank" href="">
                            <div style="height:150px">
                                <div style="height:150px;width:150px">
                                    <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                                </div>
                            </div>
                            <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                            <p class="item-summary">5000 mAh Battery</p>
                            <p class="item-summary">Now ₹10999</p>
                        </a>
                    </div>
                </div>
                
                <div>
                    <div class="item">
                        <a title="5000 mAh Battery" target="_blank" href="">
                            <div style="height:150px">
                                <div style="height:150px;width:150px">
                                    <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                                </div>
                            </div>
                            <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                            <p class="item-summary">5000 mAh Battery</p>
                            <p class="item-summary">Now ₹10999</p>
                        </a>
                    </div>
                </div>
                
                <div>
                    <div class="item">
                        <a title="5000 mAh Battery" target="_blank" href="">
                            <div style="height:150px">
                                <div style="height:150px;width:150px">
                                    <img src="images/mobile.jpeg" alt="Mobile" style="height:150px;width:80px">
                                </div>
                            </div>
                            <p class="item-header">SAMSUNG Galaxy M11 (Black, 32 GB)</p>
                            <p class="item-summary">5000 mAh Battery</p>
                            <p class="item-summary">Now ₹10999</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 col-lg-3 col-3 p-0" style="background-image:url('images/ad.jpg');background-size: 100% 100%;background-repeat: no-repeat;">
                
            </div>
        </div>
    </div>
    
    
    <!--Footer Start-->
    <div class="footer">
        <div class="footerOne">
            <div class="col-xl-3 col-12">
                <h4>This is FindAll</h4>
                <p>Welcome to FindAll, a modern business directory and listing theme that’s just perfect for your new website.</p>
                <a href="" style="color: #43824f;background-color: transparent;text-decoration:none;">All New Businesses 
                    <svg version="1.1" class="eltdf-arrow-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.917px" height="18px" viewBox="0 0 17.917 18" enable-background="new 0 0 17.917 18" xml:space="preserve">
                        <circle fill="none" stroke="#000000" stroke-miterlimit="10" cx="8.997" cy="9.012" r="7.875"></circle>
                        <line fill="none" stroke="#000000" stroke-linecap="round" stroke-miterlimit="10" x1="7.572" y1="12.547" x2="11.172" y2="8.947"></line>
                        <line fill="none" stroke="#000000" stroke-linecap="round" stroke-miterlimit="10" x1="7.572" y1="5.289" x2="11.172" y2="8.889"></line>
                    </svg>
                </a>
            </div>
            <div class="col-xl-5 col-12">
                <h4>Featured Cities</h4>
                <div style="display:flex;flex-wrap:wrap;">
                    <div class="col-xl-4 col-lg-4 col-md-4 p-0 footer-items">
                        <li><a href="">Granollers, ES</a></li>
                        <li><a href="">Newcastle, UK</a></li>
                        <li><a href="">Saint-Étienne, FR</a></li>
                        <li><a href="">Stockholm, SE</a></li>
                        <li><a href="">Frankfurt, DE</a></li>
                        <li><a href="">Rimini, IT</a></li>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 p-0 footer-items">
                        <li><a href="">Granollers, ES</a></li>
                        <li><a href="">Newcastle, UK</a></li>
                        <li><a href="">Saint-Étienne, FR</a></li>
                        <li><a href="">Stockholm, SE</a></li>
                        <li><a href="">Frankfurt, DE</a></li>
                        <li><a href="">Rimini, IT</a></li>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 p-0 footer-items">
                        <li><a href="">Granollers, ES</a></li>
                        <li><a href="">Newcastle, UK</a></li>
                        <li><a href="">Saint-Étienne, FR</a></li>
                        <li><a href="">Stockholm, SE</a></li>
                        <li><a href="">Frankfurt, DE</a></li>
                        <li><a href="">Rimini, IT</a></li>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-12">
                <h4>Featured States</h4>
                <div style="display:flex;flex-wrap:wrap;">
                    <div class="col-xl-6 col-lg-4 col-md-4 col-12 p-0 footer-items">
                        <li><a href="">Spain</a></li>
                        <li><a href="">France</a></li>
                        <li><a href="">United States</a></li>
                        <li><a href="">England</a></li>
                        <li><a href="">Italy</a></li>
                        <li><a href="">Sweden</a></li>
                    </div>
                    <div class="col-xl-6 col-lg-4 col-md-4 col-12 p-0 footer-items">
                        <li><a href="">Scotland</a></li>
                        <li><a href="">Germany</a></li>
                        <li><a href="">Nederland</a></li>
                        <a href="" style="color: #43824f;background-color: transparent;text-decoration:none;margin-top:10px;">All States 
                            <svg version="1.1" class="eltdf-arrow-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17.917px" height="18px" viewBox="0 0 17.917 18" enable-background="new 0 0 17.917 18" xml:space="preserve">
                                <circle fill="none" stroke="#000000" stroke-miterlimit="10" cx="8.997" cy="9.012" r="7.875"></circle>
                                <line fill="none" stroke="#000000" stroke-linecap="round" stroke-miterlimit="10" x1="7.572" y1="12.547" x2="11.172" y2="8.947"></line>
                                <line fill="none" stroke="#000000" stroke-linecap="round" stroke-miterlimit="10" x1="7.572" y1="5.289" x2="11.172" y2="8.889"></line>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerTwo">
            <div class="footer-icons-section">
                <i class="fa fa-facebook footer-icon"></i>
                <i class="fa fa-instagram footer-icon"></i>
                <i class="fa fa-twitter footer-icon"></i>
                <i class="fa fa-linkedin footer-icon"></i>
            </div>
            <div class="footer-nav-a">
                <h6><a href=""><span style="color: #f6f6f6;">Discover Process</span></a></h6>
                <h6><a href=""><span style="color: #f6f6f6;">Get To Know Us</span></a></h6>
                <h6><a href=""><span style="color: #f6f6f6;">Pricing Packages</span></a></h6>
                <h6><a href=""><span style="color: #f6f6f6;">User Dashboard</span></a></h6>
                <h6><a href=""><span style="color: #f6f6f6;">Contact Us Now</span></a></h6>
            </div>
            <div class="footer-nav-a">
                <h6 style="padding-right: 5px;"><a href=""><span style="color: #f6f6f6;">1879 Madison Ave New York, US,</span></a></h6>
                <h6><a href=""><span style="color: #f6f6f6;">findall@qodethemes.com</span></a></h6>
            </div>
        </div>
    </div>
</body>
<script>
    $(".hamburger").click(function(){
        $(".mobileMenuItems").toggle();
    });
    $("#mmItemOne").click(function(){
        $("#mmItemsOne").toggle();
    });
    $("#mmItemTwo").click(function(){
        $("#mmItemsTwo").toggle();
    });
    $("#mmItemThree").click(function(){
        $("#mmItemsThree").toggle();
    });
    $("#mmItemFour").click(function(){
        $("#mmItemsFour").toggle();
    });
    $("#mmItemFive").click(function(){
        $("#mmItemsFive").toggle();
    });
    $("#mmItemSix").click(function(){
        $("#mmItemsSix").toggle();
    });
</script>
</html>